<div style="display: none">
    <input name="utm_campaign" type="text" value="">
    <input name="utm_source" type="text" value="">
    <input name="utm_medium" type="text" value="">
    <input name="utm_content" type="text" value="">
    <input name="utm_term" type="text" value="">
</div>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#3ed2a7">
    <title>AYIMUN</title>
    <link rel="shortcut icon" href="{{url('/')}}/assets/images/logo/AYIMUNLOGO3(85x85)color.png" />

    <link rel="stylesheet" href="{{url('/')}}/assets/css/liquid-icon.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/assets/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/assets/css/theme-vendors.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/assets/css/theme.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/assets/css/customhome.css?v=1.5" />

    <script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-MXB4C94');
    </script>

    <script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 10350237;
    window.__lc.chat_between_groups = false;
    (function() {
        var lc = document.createElement('script');
        lc.type = 'text/javascript';
        lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') +
            'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(lc, s);
    })();
    </script> <noscript> <a href="https://www.livechatinc.com/chat-with/10350237/">Chat with us</a>, powered by <a
            href="https://www.livechatinc.com/?welcome" rel="noopener" target="_blank">LiveChat</a> </noscript>
</head>

<body data-mobile-nav-trigger-alignment="right" data-mobile-nav-align="center" data-mobile-nav-style="minimal"
    data-mobile-nav-scheme="gray" data-mobile-header-scheme="gray" data-mobile-nav-breakpoint="1199">

    @yield('content')

    <script src="{{url('/')}}/assets/js/jquery.min.js"></script>

    <script src="{{url('/')}}/assets/js/theme-vendors.js"></script>
    <script src="{{url('/')}}/assets/js/theme.min.js"></script>
</body>

</html>